#import libraries
from __future__ import print_function, division
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import signal, misc

fs = 8000 #sampling frequency
sig = np.loadtxt(open("records/voice1.txt")) #load file
length_sig = len(sig) #length of file
array1 = np.arange(length_sig) #vector of size length of file
t = array1/fs #time vector

#plot of original signal
plt.figure(1)
plt.plot(t,sig)
plt.title('Original Signal')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.show(block=False)

#plot of FFT of original signal
sig_fft = abs(np.fft.fft(sig)) #fft
sig_freq = np.fft.fftfreq(length_sig, d=(1/8000)) #frequency axis
plt.figure(2)
plt.plot(sig_freq,sig_fft)
plt.title('FFT of Original Signal')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.show(block=False)

#create vector with high frequency white noise
mu, sigma = 0, 0.1 # mean and standard deviation
noise = np.random.normal(mu, sigma, len(sig))

#plot of high frequency white noise
plt.figure(3)
plt.plot(t,noise)
plt.title('High Frequency Noise (Gaussian white noise)')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.show(block=False)

#plot of FFT of high frequency white noise
noise_fft = abs(np.fft.fft(noise))
plt.figure(4)
plt.plot(sig_freq,noise_fft)
plt.title('FFT of High Frequency Noise (Gaussian white noise)')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.show(block=False)

#signal with added white noise
signal_noise = sig + noise 

#plot of signal with added noise
plt.figure(5)
plt.plot(t,signal_noise)
plt.title('Signal with Added High Frequency Noise')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.show(block=False)

#plot of FFT of noisy signal
signal_noise_fft = abs(np.fft.fft(signal_noise))
plt.figure(6)
plt.plot(sig_freq,signal_noise_fft)
plt.title('FFT of Signal with Added High Frequency Noise')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.show(block=False)

#moving average filter function
def running_mean(x, windowSize):
   cumsum = np.cumsum(np.insert(x, 0, 0)) 
   return (cumsum[windowSize:] - cumsum[:-windowSize]) / windowSize 

#moving average filter applied to signal, window size of 5
MAfilter = running_mean(signal_noise, 5)

#plot of graph filtered with moving average filter
plt.figure(7)  
length_MA_filtered = len(MAfilter)
array2 = np.arange(length_MA_filtered)
t2 = array2/fs
plt.plot(t2,MAfilter)
plt.title('Filtered Signal - Moving Average')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.show(block=False)

#plot of FFt of filtered signal
MA_filtered_fft = abs(np.fft.fft(MAfilter))
plt.figure(8)
sig_freq = np.fft.fftfreq(length_MA_filtered, d=(1/8000))
plt.plot(sig_freq,MA_filtered_fft)
plt.title('FFT of filtered signal - Moving Average Filter')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.show(block=False)

#derivative filter
b = np.array([1, 0.618, 1]) #coefficient b
a = np.array([2.618]) #coefficient a
Deriv_filter = signal.lfilter(b, a, MAfilter) #Derivative Filter

#plot of graph filtered with moving average filter and derivative filter
plt.figure(9)  
length_deriv_filtered = len(Deriv_filter)
array3 = np.arange(length_deriv_filtered)
t3 = array2/fs
plt.plot(t3,Deriv_filter)
plt.title('Filtered Signal - Moving Average and Derivative')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.show(block=False)

#plot of FFt of filtered signal
filtered_deriv_fft = abs(np.fft.fft(Deriv_filter))
plt.figure(10)
sig_freq = np.fft.fftfreq(length_deriv_filtered, d=(1/8000))
plt.plot(sig_freq,filtered_deriv_fft)
plt.title('FFT of filtered signal - Moving Average and Derivative Filter')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.show()
