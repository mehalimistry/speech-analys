#import libraries
from __future__ import print_function, division
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import signal as sp
import os

fs = 8000 #sampling frequency 
file_path = "\records" #file path
filename = "voice" #root filename
signal = [[0 for i in range(2)] for j in range(208)] #initalize 2d array with all speech signals

#load speech signals into array
for x in range(1, 208):
    path = filename + str(x) + '.txt' 
    txtfile = os.path.join("records", path) #path and filename
    signal[x][0]= x #1st column, incrementing integers
    signal[x][1]= np.loadtxt(open(txtfile)) #2nd column, speech signals
    
#debug, remove later
plt.plot(signal[2][1])
plt.show()

#create vector with high frequency white noise
mu, sigma = 0, 0.1 # mean and standard deviation

#initialize noise vector
noise = []
for i in range(208):
    noise.append(i)

#signal with added high frequency white noise
for x in range(1,208):
    s = signal[x][1]
    noise[x] = np.random.normal(mu, sigma, len(s))
    s = s + noise[x] 
    signal[x][1] = s

#debug, remove later
plt.plot(signal[2][1])
plt.show()

#moving average filter function
def running_mean(x, windowSize):
   cumsum = np.cumsum(np.insert(x, 0, 0)) 
   return (cumsum[windowSize:] - cumsum[:-windowSize]) / windowSize 

#filters
for x in range(1,208):
    s = signal[x][1]
    MAfilter = running_mean(s, 5) #moving average filter
    b = np.array([1, 0.618, 1]) #deriv coeffcient, b
    a = np.array([2.618]) #deriv coeffcient, a
    Deriv_filter = sp.lfilter(b, a, MAfilter) #derivative filter
    signal[x][1] = Deriv_filter

#debug, remove later
plt.plot(signal[2][1])
plt.show()
